package net.flowas.secure.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;

import java.util.ArrayList;

import javax.persistence.Cacheable;
import javax.persistence.ManyToOne;
//import javax.validation.constraints.NotNull;
//import com.google.common.collect.Lists;

/**
 * 用户.
 *
 * @author calvin
 */
@Entity
@Table(name = "ss_user")
// 默认的缓存策略.
@Cacheable(true)
public class User extends IdEntity {

    //@NotNull
    private String loginName;
    @Transient
    private String plainPassword;
    private String password;
    private String salt;
    //@NotNull
    private String name;
    private String email;
    private String status;

    // 多对多定义
    @ManyToMany
    @JoinTable(name = "ss_user_role", joinColumns = {
        @JoinColumn(name = "user_id")}, inverseJoinColumns = {
        @JoinColumn(name = "role_id")})
	// Fecth策略定义
    //@Fetch(FetchMode.SUBSELECT)
    // 集合按id排序
    @OrderBy("id ASC")
	// 缓存策略
    //@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private List<Role> roleList = new ArrayList(); // 有序的关联对象集合

    @ManyToOne
    private Group group;

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPlainPassword() {
        return plainPassword;
    }

    public void setPlainPassword(String plainPassword) {
        this.plainPassword = plainPassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //@Email
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Role> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<Role> roleList) {
        this.roleList = roleList;
    }

    /**
     * @return the group
     */
    public Group getGroup() {
        return group;
    }

    /**
     * @param group the group to set
     */
    public void setGroup(Group group) {
        this.group = group;
    }
    @Override
    public String toString() {
    	return this.loginName;
    }
}
