package net.flowas.modulecart.rest.impl;

import java.util.HashMap;

import net.flowas.modulecart.rest.ConfigerationEndpoint;

/**
 * 
 */
public class ConfigerationImpl implements ConfigerationEndpoint {
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.flowas.modulecart.rest.impl.ConfigerationEndpoint#listAll(java.lang.
	 * Integer, java.lang.Integer)
	 */
	@Override
	public HashMap listAll(Integer startPosition, Integer maxResult) {
		HashMap map = new HashMap();
		HashMap plugins = new HashMap();
		HashMap secure = new HashMap();
		secure.put("url", "login.jsp");
		secure.put("pageFragment", "loginInfo.jsp");
		secure.put("label", "登录");
		plugins.put("secure", secure);
		map.put("plugins", plugins);
		return map;
	}
}